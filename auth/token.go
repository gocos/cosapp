package auth

import (
	"encoding/json"
	"net/http"
	"strings"
	"time"

	jose "github.com/dvsekhvalnov/jose2go"
	"github.com/pkg/errors"
	"gitlab.com/gocos/cosapp/core"
)

type TokenPayload struct {
	App  string `json:"app,omitempty"`
	User string `json:"user,omitempty"`
}

type Token struct {
	AppID   string
	UserID  string
	IssueAt time.Time
	Expire  time.Time
	Access  string
	Scope   Scope
}

// Get token from request
func (t *Token) FromRequest(r *http.Request) error {

	if str := r.Header.Get("Authorization"); str != "" {

		bearer := strings.Split(str, " ")

		if len(bearer) != 2 {
			return core.NewError("No bearer for authorization token", http.StatusUnauthorized, ERR_AUTH_NO_BEARER)
		}

		t.Decode(bearer[1])
		return nil
	}

	if str := r.URL.Query().Get("access_token"); str != "" {
		t.Decode(str)
		return nil
	}

	return core.NewError("No access token provided in request", http.StatusUnauthorized, ERR_AUTH_NO_BEARER)
}

// Decode access token
func (t *Token) Decode(accessToken string) error {

	if accessToken == "" {
		return errors.New("Access token is empty")
	}

	payload, headers, err := jose.Decode(accessToken, []byte("secret"))

	if err != nil {
		return errors.Wrap(err, "Failed to decode json token")
	}

	eatv, eatexist := headers["eat"]

	if !eatexist {
		panic("Expire value is missing from token")
	}

	eatn := int64(eatv.(float64))

	eatt := time.Unix(eatn, 0)

	if eatt.Before(time.Now()) {
		return core.NewError("Token expired", http.StatusUnauthorized, ERR_AUTH_EXPIRED)
	}

	scopev, hasScope := headers["scope"]
	if !hasScope {
		return core.NewError("No scope provided for token", http.StatusUnauthorized, ERR_AUTH_NO_SCOPE)
	}

	scope, scopeIsStr := scopev.(string)
	if !scopeIsStr {
		return core.NewError("Expected string for token scope", http.StatusUnauthorized, ERR_AUTH_SCOPE_NOT_STR)
	}

	tokenPayload := &TokenPayload{}

	if err := json.Unmarshal([]byte(payload), tokenPayload); err != nil {
		return core.NewError("Failed to decode token payload", http.StatusUnauthorized, ERR_AUTH_INV_PAYLOAD)
	}

	t.AppID = tokenPayload.App
	t.UserID = tokenPayload.User
	t.Expire = eatt
	t.Scope = Scope(scope)
	t.Access = accessToken

	return nil
}

func getToken(r *http.Request) (*Token, error) {
	t := &Token{}
	err := t.FromRequest(r)
	return t, err
}

func createAccessToken(tokenPayload TokenPayload, scope Scope) (token string, err error) {

	iat := time.Now()
	eat := time.Now()

	eat = eat.Add(time.Hour * 24)

	issued := jose.Header("iat", iat.Unix())
	expire := jose.Header("eat", eat.Unix())
	scopeHeader := jose.Header("scope", scope)

	payload, err := json.Marshal(tokenPayload)
	if err != nil {
		return "", err
	}

	accessToken, err := jose.Sign(
		string(payload),
		jose.HS256,
		[]byte("secret"),
		issued,
		expire,
		scopeHeader,
	)

	if err != nil {
		return "", core.NewError(err, ERR_CREATE_TOKEN)
	}

	return accessToken, nil
}
