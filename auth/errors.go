package auth

const (
	// Failed to create password
	ERR_AUTH_EXPIRED = 1000 + iota

	// Authorization token requires scope
	ERR_AUTH_NO_SCOPE

	// Invalid scope provided in authorization token
	ERR_AUTH_SCOPE_NOT_STR

	// Expected 'auth' scope
	ERR_AUTH_SCOPE

	// Expected custom scope
	ERR_AUTH_SCOPE_EXPECTED

	// Expected int64
	ERR_AUTH_INV_PAYLOAD

	// No user found for provided authorization token
	ERR_AUTH_NO_USER

	ERR_AUTH_INACTIVE

	// Authorization header has no bearer
	ERR_AUTH_NO_BEARER

	// No authorization header found
	ERR_AUTH_NO_AUTH

	// Failed to create auth token
	ERR_CREATE_TOKEN
)
