package auth

import (
	"net/http"
	"strings"
	"time"

	"gitlab.com/gocos/cosapp/core"
)

type User interface {
	AuthGetID() string
	AuthGetIdentifier() string
	AuthCheckPassword(password string) bool
	AuthAccountActive() bool
	AuthMetadata() map[string]string
}

type UserFetcher interface {
	GetByID(id string) (User, error)
	GetByIdentifier(identity string) (User, error)
}

type AuthOptions struct {
	UserFetch UserFetcher
}

type Scope string

func (s Scope) String() string {
	return string(s)
}

func (s Scope) List() []string {
	return strings.Split(string(s), ",")
}

func (s Scope) Resource() string {
	return strings.Split(string(s), ":")[0]
}

func (s Scope) Action() string {
	a := strings.Split(string(s), ":")
	if len(a) == 2 {
		return a[1]
	}
	return ""
}

func (s Scope) Match(matching Scope) bool {

	for _, scope := range s.List() {
		for _, match := range matching.List() {
			if scope == match {
				return true
			}
		}
	}

	return false
}

type UserCache struct {
	User   User
	Expire time.Time
}

type Auth struct {
	options *AuthOptions

	// cache users by token
	cache map[string]*UserCache

	reqTokenCache map[*http.Request]*Token
}

func GetAuth(options *AuthOptions) *Auth {
	return &Auth{
		options,
		make(map[string]*UserCache),
		make(map[*http.Request]*Token),
	}
}

func (a *Auth) Login(identity, password string) (User, error) {

	user, err := a.options.UserFetch.GetByIdentifier(identity)

	if err != nil {
		return nil, core.NewError("No user", http.StatusUnauthorized, ERR_AUTH_NO_USER)
	}

	if !user.AuthAccountActive() {
		return nil, core.NewError("Account innactive", http.StatusUnauthorized, ERR_AUTH_INACTIVE)
	}

	if !user.AuthCheckPassword(password) {
		return nil, core.NewError("Invalid password", http.StatusUnauthorized, 1001)
	}

	return user, err
}

// Get user from request authorization
func (a *Auth) GetUser(r *http.Request) (user User, err error) {

	token := &Token{}

	if err := token.FromRequest(r); err != nil {
		return nil, err
	}

	if cache, cached := a.cache[token.Access]; cached {
		if cache.Expire.After(time.Now()) {
			return cache.User, nil
		}
	}

	user, err = a.options.UserFetch.GetByID(token.UserID)

	if err != nil {
		return nil, core.NewError("No user for provided token", http.StatusUnauthorized, ERR_AUTH_NO_USER)
	}

	a.cache[token.Access] = &UserCache{User: user, Expire: token.Expire}

	return
}
