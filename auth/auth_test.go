package auth

import (
	"bytes"
	"errors"
	"net/http"
	"testing"
)

type MockUser struct {
	ID       string
	Email    string
	Password string
	Active   bool
}

func (mu *MockUser) AuthGetID() string {
	return mu.ID
}

func (mu *MockUser) AuthGetIdentifier() string {
	return mu.Email
}

func (mu *MockUser) AuthCheckPassword(password string) bool {
	return mu.Password == password
}

func (mu *MockUser) AuthAccountActive() bool {
	return mu.Active
}

func (mu *MockUser) AuthMetadata() map[string]string {
	return nil
}

type MockUserFetch struct {
	mu *MockUser
}

func (muf *MockUserFetch) GetByID(id string) (User, error) {

	if muf.mu.ID == id {
		return muf.mu, nil
	}

	return nil, errors.New("No user")
}

func (muf *MockUserFetch) GetByIdentifier(identity string) (User, error) {

	if muf.mu.Email == identity {
		return muf.mu, nil
	}

	return nil, errors.New("No user")
}

func TestAuthLogin(t *testing.T) {

	a := GetAuth(&AuthOptions{
		UserFetch: &MockUserFetch{
			&MockUser{
				ID:       "1",
				Email:    "user",
				Password: "pass",
				Active:   true,
			},
		},
	})

	user, err := a.Login("user", "pass")

	if err != nil {
		t.Error(err)
		return
	}

	if user.AuthGetID() != "1" {
		t.Error("Expected identifier")
	}

	_, err = a.Login("user", "wrong_pass")

	if err == nil {
		t.Error(err)
		return
	}
}

func TestAuthLoginInactive(t *testing.T) {

	a := GetAuth(&AuthOptions{
		UserFetch: &MockUserFetch{
			&MockUser{
				ID:       "1",
				Email:    "user",
				Password: "pass",
				Active:   false,
			},
		},
	})

	_, err := a.Login("user", "pass")

	if err == nil {
		t.Error(err)
		return
	}
}

func TestAuthLoginNoUser(t *testing.T) {

	a := GetAuth(&AuthOptions{
		UserFetch: &MockUserFetch{
			&MockUser{
				ID:       "1",
				Email:    "user",
				Password: "pass",
				Active:   false,
			},
		},
	})

	_, err := a.Login("x", "b")

	if err == nil {
		t.Error(err)
		return
	}
}

func TestAuthGetUserFromRequest(t *testing.T) {

	a := GetAuth(&AuthOptions{
		UserFetch: &MockUserFetch{
			&MockUser{
				ID:       "1",
				Email:    "user",
				Password: "pass",
				Active:   false,
			},
		},
	})

	at, err := createAccessToken(TokenPayload{"1", "1"}, "auth")

	if err != nil {
		t.Error(err)
		return
	}

	r, err := http.NewRequest(
		"GET",
		"http://a/?access_token="+at,
		bytes.NewBufferString(""),
	)

	user, err := a.GetUser(r)

	if err != nil {
		t.Error(err)
		return
	}

	if user.AuthGetID() != "1" {
		t.Fail()
		return
	}

	if user.AuthGetIdentifier() != "user" {
		t.Fail()
		return
	}
}
