package websocket

import (
	"encoding/json"
	"fmt"
	"reflect"
	"time"

	"github.com/pkg/errors"
)

type EventData map[string]interface{}

type Event struct {
	Type   string      `json:"type"`
	Data   EventData   `json:"data,omitempty"`
	Time   int64       `json:"time,omitempty"`
	SeqID  int64       `json:"seqid,omitempty"`
	Source *Connection `json:"-"`
}

func (e Event) String() string {
	return fmt.Sprintf("[Event(%s : %d) %v]", e.Type, e.Time, e.Data)
}

func (e Event) MustHave(keys ...string) {

	for _, key := range keys {

		if !e.Has(key) {

			msg := fmt.Sprintf("Field '%s' is required for event: %s", key, e.Type)

			e.Source.send <- Event{
				Type: "error",
				Data: EventData{
					"seqid":   e.SeqID,
					"event":   e.Type,
					"message": msg,
					"code":    0,
				},
			}

			panic(msg)
		}
	}

}

func (e Event) Has(key string) (exist bool) {
	if e.Data == nil {
		return false
	}

	_, exist = e.Data[key]

	return exist
}

func (e Event) GetString(key string) string {
	if e.Data == nil {
		return ""
	}

	if v, exist := e.Data[key]; exist {
		return v.(string)
	}

	return ""
}

func (e Event) GetBool(key string, def bool) bool {
	if e.Data == nil {
		return def
	}

	if v, exist := e.Data[key]; exist {
		return v.(bool)
	}

	return def
}

func (e Event) GetInt(key string) int {
	if e.Data == nil {
		return 0
	}

	if v, exist := e.Data[key]; exist {
		return int(v.(float64))
	}

	return 0
}

func (e Event) GetFloat(key string) float64 {
	if e.Data == nil {
		return 0
	}

	if v, exist := e.Data[key].(float64); exist {
		return v
	}

	return 0
}

func (e Event) Respond(tip string, data EventData) {
	if e.Source != nil {
		e.Source.Send(Event{
			Type:  tip,
			Data:  data,
			Time:  time.Now().Unix(),
			SeqID: e.SeqID,
		})
	}
}

func (e Event) Error(message string, code int) {
	if e.Source != nil {
		e.Source.Send(Event{
			Type: "error",
			Data: EventData{
				"seqid":   e.SeqID,
				"event":   e.Type,
				"message": message,
				"code":    code,
			},
		})

		//time.AfterFunc(time.Second, func() { e.Source.Close() })
	}
}

func (a EventData) Equal(b EventData) (equal bool) {
	for _, key := range reflect.ValueOf(a).MapKeys() {
		bk, be := b[key.String()]
		if !be {
			return false
		}

		if fmt.Sprintf("%v", a[key.String()]) != fmt.Sprintf("%v", bk) {
			return false
		}
	}

	return true
}

func (a EventData) Marshal(to interface{}) (err error) {
	marshal, err := json.Marshal(a)
	return json.Unmarshal(marshal, to)
}

func (a EventData) MarshalKey(key string, to interface{}) (err error) {

	mv := reflect.ValueOf(a).MapIndex(reflect.ValueOf(key))

	if !mv.IsValid() {
		return errors.Errorf("Missing key '%s' from event data", key)
	}

	marshal, err := json.Marshal(mv.Interface())

	return json.Unmarshal(marshal, to)
}
