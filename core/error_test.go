package core

import "testing"

func TestError(t *testing.T) {

	e := NewError("errorx", 404, 1000)

	if e.Error() != "errorx" {
		t.Fail()
	}

	if e.Code() != 1000 {
		t.Fail()
	}

	if e.Status() != 404 {
		t.Fail()
	}
}
