package core

import (
	"bytes"
	"encoding/json"
	"net/http"
)

type Error struct {
	message string
	status  int
	code    int
}

func (err *Error) Status() int {
	return err.status
}

func (err *Error) Code() int {
	return err.code
}

func (err *Error) Error() string {
	return err.message
}

func (err *Error) MarshalJSON() ([]byte, error) {
	return json.Marshal(map[string]interface{}{"error": err.message, "code": err.code})
}

type ToString interface {
	String() string
}

func NewError(messageOrErr interface{}, codes ...int) *Error {

	var message string

	if err, iserr := messageOrErr.(error); iserr {
		message = err.Error()
	} else if str, isstr := messageOrErr.(string); isstr {
		message = str
	} else if v, canCast := messageOrErr.(ToString); canCast {
		message = v.String()
	} else {
		return &Error{message: "Invalid http error construct", code: -1, status: 500}
	}

	setCodeOrStatus := func(err *Error, n int) {
		if http.StatusText(n) != "" {
			err.status = n
		} else {
			err.code = n
		}
	}

	err := &Error{message: message}

	if len(codes) == 1 {
		setCodeOrStatus(err, codes[0])
	}

	if len(codes) == 2 {
		setCodeOrStatus(err, codes[0])
		setCodeOrStatus(err, codes[1])
	}

	return err
}

// Render a new error to response
// Opts variants:
// - [code]
// - [status]
// - [code, status]
// - [status, code]
//
// code - represents error code number
// status - http status code
func RenderError(w http.ResponseWriter, errv interface{}, codes ...int) {

	var message string
	var err *Error

	if err, iserr := errv.(error); iserr {
		message = err.Error()
	}

	if msg, str := errv.(string); str {
		message = msg
	}

	if v, ok := errv.(*Error); ok {
		err = v
	}

	if v, ok := errv.(Error); ok {
		err = &v
	}

	if message == "" && err == nil {
		panic("Expected error or string")
	}

	if err == nil {
		err = NewError(message, codes...)
	}

	buf := &bytes.Buffer{}
	enc := json.NewEncoder(buf)
	enc.SetEscapeHTML(true)

	if err := enc.Encode(err); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json; charset=utf-8")

	if err.status >= http.StatusContinue && err.status <= http.StatusNetworkAuthenticationRequired {
		w.WriteHeader(err.status)
	}

	if _, err := w.Write(buf.Bytes()); err != nil {
		panic(err)
	}
}
