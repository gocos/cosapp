package websocket

import (
	"fmt"
	"log"
	"net/http"
	"strings"
	"sync"
	"time"

	ws "github.com/gorilla/websocket"
)

type EventHandler func(event Event, engine *Engine)
type EngineHandler func(c *Connection)

type EngineOptions struct {
	GetUser func(r *http.Request) (User, error)
}

type Engine struct {
	Options *EngineOptions

	handlers map[string][]EventHandler

	internalHandlers map[string][]EngineHandler

	// Registered connections.
	connections map[*Connection]bool

	// Inbound messages from the connections.
	broadcast chan Event

	// Register requests from the connections.
	register chan *Connection

	// Unregister requests from connections.
	unregister chan *Connection

	shutdown chan int

	hubMux sync.Mutex
}

func NewEngine(options *EngineOptions) *Engine {

	engine := &Engine{
		options,
		make(map[string][]EventHandler, 0),
		make(map[string][]EngineHandler, 0),
		make(map[*Connection]bool, 0),
		make(chan Event),
		make(chan *Connection),
		make(chan *Connection),
		make(chan int),
		sync.Mutex{},
	}

	go engine.Run()

	return engine
}

func (h *Engine) Count() (count int) {
	return len(h.connections)
}

func (h *Engine) Info() (info interface{}) {

	uniqueOnline := make(map[string]interface{})
	for c := range h.connections {
		uniqueOnline[c.user.GetID()] = c.user.GetName()
	}

	online := make([]interface{}, 0)
	for _, v := range uniqueOnline {
		online = append(online, v)
	}

	return map[string]interface{}{
		"online": online,
	}
}

func (h *Engine) Register(c *Connection) {
	h.register <- c
}

func (h *Engine) User(id string) *Connection {
	for c := range h.connections {
		if c.user != nil && c.user.GetID() == id {
			return c
		}
	}
	return nil
}

func (h *Engine) NotifyOnlinePresence(c *Connection, online bool) {

	if c == nil || c.user == nil {
		panic("Unexpected nil connection or user")
	}

	users := make([]string, 0)
	for c := range h.connections {
		users = append(users, c.user.GetID())
	}

	event := Event{
		Type: "presence",
		Data: EventData{
			"users": users,
		},
	}

	for c := range h.connections {
		c.send <- event
	}
}

// Run starts handling connections
func (engine *Engine) Run() {
	for {
		select {
		case c := <-engine.register:
			engine.connections[c] = true
			log.Println("Socket: User connected: ", c.GetUser().GetID())
			engine.NotifyOnlinePresence(c, true)
			engine.internalEngineTrigger("onenter", c)
		case c := <-engine.unregister:

			engine.internalEngineTrigger("onleave", c)

			if _, ok := engine.connections[c]; !ok {
				break
			}

			log.Println("Socket: User disconnected: ", c.GetUser().GetID())

			// FIXME:
			//engine.RouteEvent(Event{Type: "system.user.leave", Source: c})
			delete(engine.connections, c)

			c.Lock()
			close(c.send)
			c.closed = true
			c.Unlock()

			engine.NotifyOnlinePresence(c, false)
		case m := <-engine.broadcast:
			for c := range engine.connections {
				select {
				case c.send <- m:
				default:
					close(c.send)
					c.closed = true
					delete(engine.connections, c)
				}
			}
		case <-engine.shutdown:
			log.Println("Hub gracefully stopped")
			break
		}
	}
}

func (h *Engine) NotifyOnlinePresenceTo(c *Connection) {

	users := make([]string, 0)
	for c := range h.connections {
		if c.user == nil {
			panic("IMAError: User expected")
		}
		users = append(users, c.user.GetID())
	}

	c.send <- Event{
		Type: "presence",
		Data: EventData{
			"users": users,
		},
	}
}

// Check if origin is allowed
func checkOrigin(r *http.Request) bool {

	actualOrigin := r.Header.Get("origin")

	origins := ""

	if origins == "" {
		return true
	}

	for _, origin := range strings.Split(origins, ",") {
		if origin == actualOrigin {
			return true
		}
	}

	log.Println("Origin not allowed:", actualOrigin)

	return false
}

func (engine *Engine) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	defer func() {
		if err := recover(); err != nil {
			fmt.Println("Socket error:", err)
		}
	}()

	user, err := engine.Options.GetUser(r)

	if err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	if r.Method != "GET" {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}

	var upgrader = ws.Upgrader{
		ReadBufferSize:  maxMessageSize,
		WriteBufferSize: maxMessageSize,
		CheckOrigin:     checkOrigin,
	}

	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	c := &Connection{
		engine: engine,
		ws:     ws,
		send:   make(chan Event, 256),
		user:   user,
	}

	engine.Register(c)

	go c.writePump()

	c.readPump()
}

func (engine *Engine) OnLeave(cb EngineHandler) func() {
	return engine.internalEngineListen("onleave", cb)
}

func (engine *Engine) OnEnter(cb EngineHandler) func() {
	return engine.internalEngineListen("onenter", cb)
}

func (engine *Engine) internalEngineTrigger(event string, c *Connection) {
	for name, handlers := range engine.internalHandlers {
		if name == event {
			for _, handler := range handlers {
				handler(c)
			}
		}
	}
}

func (engine *Engine) internalEngineListen(event string, handler EngineHandler) (unregister func()) {

	engine.hubMux.Lock()

	if _, e := engine.internalHandlers[event]; !e {
		engine.internalHandlers[event] = make([]EngineHandler, 0)
	}

	index := len(engine.internalHandlers[event]) - 1

	engine.internalHandlers[event] = append(engine.internalHandlers[event], handler)

	unregister = func() {
		engine.hubMux.Lock()

		engine.internalHandlers[event] = append(
			engine.internalHandlers[event][:index],
			engine.internalHandlers[event][index+1:]...,
		)

		engine.hubMux.Unlock()
	}

	engine.hubMux.Unlock()

	return
}

func (engine *Engine) Listen(etype string, handler EventHandler) (unregister func()) {
	engine.hubMux.Lock()

	if _, e := engine.handlers[etype]; !e {
		engine.handlers[etype] = make([]EventHandler, 0)
	}

	index := len(engine.handlers[etype]) - 1

	engine.handlers[etype] = append(engine.handlers[etype], handler)

	unregister = func() {
		engine.hubMux.Lock()

		engine.handlers[etype] = append(
			engine.handlers[etype][:index],
			engine.handlers[etype][index+1:]...,
		)

		engine.hubMux.Unlock()
	}

	engine.hubMux.Unlock()

	return
}

func (engine *Engine) UserLeave(f func(c *Connection)) {
	engine.Listen("system.user.leave", func(event Event, engine *Engine) {
		f(event.Source)
	})
}

func (engine *Engine) RouteEvent(event Event) bool {
	handlers, ok := engine.handlers[event.Type]
	if !ok {
		return false
	}

	for _, handler := range handlers {
		go handler(event, engine)
	}

	return true
}

// Notify sends event to all users if are online
func (e *Engine) Notify(event string, users []string, data EventData) {
	for c := range e.connections {
		for _, user := range users {
			if c.GetUser().GetID() == user {
				c.send <- Event{
					Type: event,
					Data: data,
					Time: time.Now().Unix(),
				}
			}
		}
	}
}

func (engine *Engine) Shutdown() {
	engine.shutdown <- 1
}
