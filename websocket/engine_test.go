package websocket

import (
	"log"
	"net"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/go-chi/chi"
	ws "github.com/gorilla/websocket"
)

type MockUser struct {
	ID   string
	Name string
}

func (mu MockUser) GetID() string {
	return mu.ID
}
func (mu MockUser) GetName() string {
	return mu.Name
}

func getMockUser(r *http.Request) (User, error) {
	return &MockUser{"1", "mock"}, nil
}

func TestWebsocket(t *testing.T) {

	e := NewEngine(&EngineOptions{
		GetUser: getMockUser,
	})

	router := chi.NewMux()
	router.Handle("/", e)

	l, err := net.Listen("tcp", "127.0.0.1:8080")
	if err != nil {
		log.Fatal(err)
	}

	s := httptest.NewUnstartedServer(router)
	s.Listener.Close()
	s.Listener = l
	s.Start()
	defer s.Close()

	conn, _, err := ws.DefaultDialer.Dial("ws://localhost:8080/", http.Header{})
	defer conn.Close()

	if err != nil {
		t.Error("Failed to dial websocket")
		return
	}

	ev := Event{}
	if err := conn.ReadJSON(&ev); err != nil {
		t.Error(err)
		return
	}

	if ev.Type != "presence" {
		t.Error("Expected presence event")
	}
}
