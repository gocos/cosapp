package auth

import (
	"bytes"
	"net/http"
	"testing"
)

func TestToken(t *testing.T) {

	at, err := createAccessToken(TokenPayload{"a", "b"}, "auth")

	if err != nil {
		t.Error(err)
		return
	}

	r, err := http.NewRequest(
		"GET",
		"http://a/?access_token="+at,
		bytes.NewBufferString(""),
	)

	if err != nil {
		t.Error(err)
		return
	}

	token := &Token{}
	if err := token.FromRequest(r); err != nil {
		t.Error(err)
		return
	}

	if token.AppID != "a" && token.UserID != "b" {
		t.Error("Expected match")
	}
}
