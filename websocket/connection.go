package websocket

import (
	"encoding/json"
	"fmt"
	"log"
	"sync"
	"time"

	ws "github.com/gorilla/websocket"
	"github.com/pkg/errors"
)

type User interface {
	GetID() string
	GetName() string
}

const (
	// Time allowed to write a message to the peer.
	writeWait = 10 * time.Second

	// Time allowed to read the next pong message from the peer.
	pongWait = 60 * time.Second

	// Send pings to peer with this period. Must be less than pongWait.
	pingPeriod = (pongWait * 9) / 10

	// Maximum message size allowed from peer.
	maxMessageSize = 16384
)

type Connection struct {
	engine *Engine

	sync.Mutex

	// WebSocket Connection
	ws *ws.Conn

	// Sending channel
	send chan Event

	user User

	closed bool
}

// Read data from connection
func (c *Connection) readPump() {
	defer func() {
		c.engine.unregister <- c
		c.ws.Close()
	}()

	c.ws.SetReadLimit(maxMessageSize)
	c.ws.SetReadDeadline(time.Now().Add(pongWait))
	c.ws.SetPongHandler(func(string) error { c.ws.SetReadDeadline(time.Now().Add(pongWait)); return nil })

	for {
		messageType, message, err := c.ws.ReadMessage()
		if err != nil {
			if ws.IsUnexpectedCloseError(err, ws.CloseGoingAway) {
				log.Printf("Error on c.ws.ReadMessage(): %v\n", err)
			}
			break
		}

		if messageType == ws.TextMessage {
			event := Event{}

			if err := json.Unmarshal(message, &event); err != nil {
				fmt.Println("Fail to unmarshal socket packet:", err.Error())
				continue
			}

			event.Source = c

			if string(event.Type) == "ping" {
				event.Respond("pong", nil)
			} else if !c.engine.RouteEvent(event) {
				log.Println("Event unknown:", event.String())
			}
		}
	}

}

func (c *Connection) writeEvent(event Event) error {

	event.Time = time.Now().Unix()

	bytes, err := json.Marshal(event)
	if err != nil {
		log.Println("Fail to encode bson bytes: ", err.Error())
		return err
	}

	if err := c.write(ws.TextMessage, bytes); err != nil {
		log.Println("Fail to send bson bytes:", string(bytes), "Err:", err.Error())
		return err
	}

	return nil
}

func (c *Connection) Close() {
	c.Lock()
	c.ws.Close()
	c.closed = true
	c.Unlock()
}

// write writes a message with the given message type and payload.
func (c *Connection) write(mt int, payload []byte) error {
	c.ws.SetWriteDeadline(time.Now().Add(writeWait))
	return c.ws.WriteMessage(mt, payload)
}

// Write data to connection
func (c *Connection) writePump() {
	ticker := time.NewTicker(pingPeriod)

	defer func() {
		ticker.Stop()
		c.ws.Close()
	}()

	for {
		select {
		case data, ok := <-c.send:
			if !ok {
				c.write(ws.CloseMessage, []byte{})
				return
			}
			c.writeEvent(data)
		case <-ticker.C:
			if err := c.write(ws.PingMessage, []byte{}); err != nil {
				return
			}
		}
	}
}

func (c *Connection) SetUser(user User) (err error) {

	if c.user != nil {
		return errors.New("user already set")
	}

	c.user = user

	return
}

func (c *Connection) GetUser() (user User) {

	if c == nil {
		panic("Unexpected")
	}

	return c.user
}

func (c *Connection) Send(event Event) (err error) {

	if c.closed {
		return errors.New("connection closed")
	}

	event.Time = time.Now().Unix()

	c.send <- event

	return
}
