module gitlab.com/gocos/cosapp

go 1.14

require (
	github.com/dvsekhvalnov/jose2go v0.0.0-20201001154944-b09cfaf05951
	github.com/go-chi/chi v1.5.1
	github.com/gorilla/websocket v1.4.2
	github.com/pkg/errors v0.9.1
	golang.org/x/crypto v0.0.0-20201203163018-be400aefbc4c
)
